FROM node:12-alpine
MAINTAINER RostislavNovikov
WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY . /app
RUN npm run build

RUN npm install --global serve
CMD serve -s dist
