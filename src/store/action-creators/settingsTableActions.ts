import { Dispatch } from 'redux';
import { SettingsTableAction, SettingsTableActionTypes } from '../../types/settingsTable';

export const settingsTableOpen = () => {
  return (dispatch: Dispatch<SettingsTableAction>) => {
    dispatch({ type: SettingsTableActionTypes.SETTINGS_TABLE_OPEN });
  };
};

export const settingsTableClose = () => {
  return async (dispatch: Dispatch<SettingsTableAction>) => {
    dispatch({ type: SettingsTableActionTypes.SETTINGS_TABLE_CLOSE });
  };
};

export const tableColumnChange = (payload: number[]) => {
  return async (dispatch: Dispatch<SettingsTableAction>) => {
    dispatch({ type: SettingsTableActionTypes.TABLE_COLUNM_CHANGE, payload });
  };
};

export const filtersOpen = () => {
  return (dispatch: Dispatch<SettingsTableAction>) => {
    dispatch({ type: SettingsTableActionTypes.FILTERS_OPEN });
  };
};

export const filtersClose = () => {
  return async (dispatch: Dispatch<SettingsTableAction>) => {
    dispatch({ type: SettingsTableActionTypes.FILTERS_CLOSE });
  };
};

export const filterAdd = (payload: any) => {
  return async (dispatch: Dispatch<SettingsTableAction>) => {
    dispatch({ type: SettingsTableActionTypes.FILTER_ADD, payload });
  };
};
