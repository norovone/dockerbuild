import * as AuthActionCreators from './authActions';
import * as MenuActionCreators from './menuActions';
import * as SettingsTableCreators from './settingsTableActions';

export default {
  ...AuthActionCreators,
  ...MenuActionCreators,
  ...SettingsTableCreators,
};
