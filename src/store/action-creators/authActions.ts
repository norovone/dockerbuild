import { Dispatch } from 'redux';
import axios from 'axios';
import { AuthAction, AuthActionTypes } from '../../types/authTypes';

export const fetchLogin = () => {
  return async (dispatch: Dispatch<AuthAction>) => {
    try {
      dispatch({ type: AuthActionTypes.AUTH_LOGIN });
      // GET JWT TOKEN
      dispatch({ type: AuthActionTypes.AUTH_LOGIN_SUCCESS, payload: true });
    } catch (e) {
      dispatch({
        type: AuthActionTypes.AUTH_LOGIN_ERROR,
        payload: 'Ошибка при авторизации пользователя: ' + e,
      });
    }
  };
};

export const fetchLogout = () => {
  return async (dispatch: Dispatch<AuthAction>) => {
    try {
      dispatch({ type: AuthActionTypes.AUTH_LOGOUT });
      // GET LOGOUT
      dispatch({ type: AuthActionTypes.AUTH_LOGOUT_SUCCESS, payload: false });
    } catch (e) {
      dispatch({
        type: AuthActionTypes.AUTH_LOGIN_ERROR,
        payload: 'Ошибка при разлогировании: ' + e,
      });
    }
  };
};
