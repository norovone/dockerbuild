import { Dispatch } from 'redux';
import { MenuAction, MenuActionTypes } from '../../types/menuTypes';

export const menuOpen = () => {
  return (dispatch: Dispatch<MenuAction>) => {
    dispatch({ type: MenuActionTypes.MENU_OPEN });
  };
};

export const menuClose = () => {
  return async (dispatch: Dispatch<MenuAction>) => {
    dispatch({ type: MenuActionTypes.MENU_CLOSE });
  };
};
