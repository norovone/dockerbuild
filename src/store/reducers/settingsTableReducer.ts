import {
  SettingsTableAction,
  SettingsTableActionTypes,
  SettingsTableState,
} from '../../types/settingsTable';
import { columnsTableChecked } from '../../assets/elemetsData/';

const initialState: SettingsTableState = {
  openSettingsTable: false,
  columnsTable: columnsTableChecked,
  openFilters: false,
  filters: [],
};

export const settingsTableReducer = (
  state = initialState,
  action: SettingsTableAction
): SettingsTableState => {
  switch (action.type) {
    case SettingsTableActionTypes.SETTINGS_TABLE_OPEN:
      return { ...state, openSettingsTable: true };

    case SettingsTableActionTypes.SETTINGS_TABLE_CLOSE:
      return { ...state, openSettingsTable: false };

    case SettingsTableActionTypes.TABLE_COLUNM_CHANGE:
      return { ...state, columnsTable: [...action.payload] };

    case SettingsTableActionTypes.FILTERS_OPEN:
      return { ...state, openFilters: true };

    case SettingsTableActionTypes.FILTERS_CLOSE:
      return { ...state, openFilters: false };

    case SettingsTableActionTypes.FILTER_ADD:
      return { ...state, filters: [...state.filters, action.payload] };

    default:
      return state;
  }
};
