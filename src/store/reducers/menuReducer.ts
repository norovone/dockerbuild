import { MenuAction, MenuActionTypes, MenuState } from '../../types/menuTypes';

const initialState: MenuState = {
  openMainMenu: false,
};

export const menuReducer = (state = initialState, action: MenuAction): MenuState => {
  switch (action.type) {
    case MenuActionTypes.MENU_OPEN:
      return { ...state, openMainMenu: true };

    case MenuActionTypes.MENU_CLOSE:
      return { ...state, openMainMenu: false };

    default:
      return state;
  }
};
