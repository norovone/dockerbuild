import { AuthAction, AuthActionTypes, AuthState } from '../../types/authTypes';

const initialState: AuthState = {
  auth: false,
  loading: false,
  error: null,
};

export const authReducer = (state = initialState, action: AuthAction): AuthState => {
  switch (action.type) {
    case AuthActionTypes.AUTH_LOGIN:
      return { ...state, loading: true };

    case AuthActionTypes.AUTH_LOGIN_SUCCESS:
      return { ...state, loading: false, auth: action.payload };

    case AuthActionTypes.AUTH_LOGIN_ERROR:
      return { ...state, loading: false, error: action.payload };

    case AuthActionTypes.AUTH_LOGOUT:
      return { ...state, loading: true };

    case AuthActionTypes.AUTH_LOGOUT_SUCCESS:
      return { ...state, loading: false, auth: action.payload };

    case AuthActionTypes.AUTH_LOGOUT_ERROR:
      return { ...state, loading: false, error: action.payload };

    default:
      return state;
  }
};
