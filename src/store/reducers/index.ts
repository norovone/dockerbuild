import { combineReducers } from 'redux';
import { authReducer } from './authReducer';
import { menuReducer } from './menuReducer';
import { settingsTableReducer } from './settingsTableReducer';

export const rootReducer = combineReducers({
  auth: authReducer,
  menu: menuReducer,
  settingsTable: settingsTableReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
