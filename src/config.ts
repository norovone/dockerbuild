export const config = {
  LOGIN: '/login',
  ORDERS: '/orders',
  HELP: '/help',
  SETTINGS: '/settings',
  PROFILE: '/profile',
};
