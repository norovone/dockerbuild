import Login from './components/Login';
import Orders from './pages/Orders';
import { config } from './config';
import Help from './pages/Help';
import Settings from './pages/Settings';
import Profile from './pages/Profile';

export const publicRoutes = [
  {
    path: config.LOGIN,
    Component: Login,
  },
];

export const privateRoutes = [
  {
    path: config.ORDERS,
    Component: Orders,
  },
  {
    path: config.HELP,
    Component: Help,
  },
  {
    path: config.SETTINGS,
    Component: Settings,
  },
  {
    path: config.PROFILE,
    Component: Profile,
  },
];
