export interface MenuState {
  openMainMenu: boolean;
}

export enum MenuActionTypes {
  MENU_OPEN = 'MENU_OPEN',
  MENU_CLOSE = 'MENU_CLOSE',
}

interface MenuOpenAction {
  type: MenuActionTypes.MENU_OPEN;
}

interface MenuCloseAction {
  type: MenuActionTypes.MENU_CLOSE;
}

export type MenuAction = MenuOpenAction | MenuCloseAction;
