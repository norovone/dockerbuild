export interface IBaseElement {
  id: string;
  label: string;
}

export interface IColumnTableOrders {
  idx: number;
  id:
    | 'number'
    | 'type'
    | 'name'
    | 'sum'
    | 'term'
    | 'manager'
    | 'status'
    | 'dateCreate'
    | 'dateChange';
  label: string;
  minWidth?: number;
  align?: 'left' | 'right' | 'inherit' | 'center' | 'justify';
}

export interface IDataTableOrders {
  number: string;
  type: string;
  name: string;
  sum: string;
  term: string;
  manager: string;
  status: string;
  dateCreate: string;
  dateChange: string;
}
