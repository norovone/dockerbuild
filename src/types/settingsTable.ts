export interface SettingsTableState {
  openSettingsTable: boolean;
  columnsTable: number[];
  openFilters: boolean;
  filters: any[];
}

export enum SettingsTableActionTypes {
  SETTINGS_TABLE_OPEN = 'SETTINGS_TABLE_OPEN',
  SETTINGS_TABLE_CLOSE = 'SETTINGS_TABLE_CLOSE',
  TABLE_COLUNM_CHANGE = 'TABLE_COLUNM_CHANGE',
  FILTERS_OPEN = 'FILTERS_OPEN',
  FILTERS_CLOSE = 'FILTERS_CLOSE',
  FILTER_ADD = 'FILTER_ADD',
  FILTER_DELETE = 'FILTER_DELETE',
}

interface SettingsTableOpenAction {
  type: SettingsTableActionTypes.SETTINGS_TABLE_OPEN;
}

interface SettingsTableCloseAction {
  type: SettingsTableActionTypes.SETTINGS_TABLE_CLOSE;
}

interface TableColunmChangeAction {
  type: SettingsTableActionTypes.TABLE_COLUNM_CHANGE;
  payload: number[];
}

interface FiltersOpenAction {
  type: SettingsTableActionTypes.FILTERS_OPEN;
}

interface FiltersCloseAction {
  type: SettingsTableActionTypes.FILTERS_CLOSE;
}

interface FilterAddAction {
  type: SettingsTableActionTypes.FILTER_ADD;
  payload: any[];
}

interface FiltersDeleteAction {
  type: SettingsTableActionTypes.FILTER_DELETE;
  payload: any[];
}

export type SettingsTableAction =
  | SettingsTableOpenAction
  | SettingsTableCloseAction
  | TableColunmChangeAction
  | FiltersOpenAction
  | FiltersCloseAction
  | FilterAddAction
  | FiltersDeleteAction;
