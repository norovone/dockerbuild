export interface AuthState {
  auth: boolean;
  loading: boolean;
  error: null | string;
}

export enum AuthActionTypes {
  AUTH_LOGIN = 'AUTH_LOGIN',
  AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS',
  AUTH_LOGIN_ERROR = 'AUTH_LOGIN_ERROR',
  AUTH_LOGOUT = 'AUTH_LOGOUT',
  AUTH_LOGOUT_SUCCESS = 'AUTH_LOGOUT_SUCCESS',
  AUTH_LOGOUT_ERROR = 'AUTH_LOGOUT_ERROR',
}

interface LoginAction {
  type: AuthActionTypes.AUTH_LOGIN;
}

interface LoginSuccessAction {
  type: AuthActionTypes.AUTH_LOGIN_SUCCESS;
  payload: boolean;
}

interface LoginErrorAction {
  type: AuthActionTypes.AUTH_LOGIN_ERROR;
  payload: string;
}

interface LogoutAction {
  type: AuthActionTypes.AUTH_LOGOUT;
}

interface LogoutSuccessAction {
  type: AuthActionTypes.AUTH_LOGOUT_SUCCESS;
  payload: boolean;
}

interface LogoutErrorAction {
  type: AuthActionTypes.AUTH_LOGOUT_ERROR;
  payload: string;
}

export type AuthAction =
  | LoginAction
  | LoginSuccessAction
  | LoginErrorAction
  | LogoutAction
  | LogoutSuccessAction
  | LogoutErrorAction;
