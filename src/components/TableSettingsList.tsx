import React, { FC, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import { colunmsTableOrders } from '../assets/elemetsData';
import { useActions } from '../hooks/useActions';
import { useTypedSelector } from '../hooks/useTypedSelector';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
  })
);

const TableSettingsList: FC = () => {
  const classes = useStyles();
  const { tableColumnChange } = useActions();
  const { columnsTable } = useTypedSelector((state) => state.settingsTable);

  const handleToggle = (value: number) => () => {
    const currentIndex = columnsTable.indexOf(value);

    const newChecked = [...columnsTable];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    tableColumnChange(newChecked);

    localStorage.setItem('columnsTable', JSON.stringify(newChecked));
  };

  return (
    <List className={classes.root}>
      {colunmsTableOrders.map((item) => {
        const labelId = `checkbox-list-label-${item.label}`;

        return (
          <ListItem key={item.id} role={undefined} dense button onClick={handleToggle(item.idx)}>
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={columnsTable.indexOf(item.idx) !== -1}
                tabIndex={-1}
                disableRipple
                inputProps={{ 'aria-labelledby': labelId }}
              />
            </ListItemIcon>
            <ListItemText id={labelId} primary={item.label} />
          </ListItem>
        );
      })}
    </List>
  );
};

export default TableSettingsList;
