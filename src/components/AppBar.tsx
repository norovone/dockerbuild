import React, { FC } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import clsx from 'clsx';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import MenuList from './MenuList';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import TuneIcon from '@material-ui/icons/Tune';
import RefreshIcon from '@material-ui/icons/Refresh';
import CreateIcon from '@material-ui/icons/Create';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useActions } from '../hooks/useActions';
import { Tooltip } from '@material-ui/core';
import { useTypedSelector } from '../hooks/useTypedSelector';

const drawerWidth = 290;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
    backgroundColor: theme.palette.grey[700],
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  toolbarButtons: {
    marginLeft: 'auto',
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
}));

const AppBarComponent: FC = () => {
  const classes = useStyles();
  const { openMainMenu } = useTypedSelector((state) => state.menu);
  const { fetchLogout, menuOpen, menuClose, settingsTableOpen } = useActions();

  const handleDrawerOpen = () => {
    menuOpen();
  };
  const handleDrawerClose = () => {
    menuClose();
  };
  const handleLogout = () => {
    fetchLogout();
    localStorage.removeItem('auth');
  };

  const handleOpenSettings = () => {
    settingsTableOpen();
  };

  return (
    <>
      <CssBaseline />
      <AppBar
        position="absolute"
        className={clsx(classes.appBar, openMainMenu && classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, openMainMenu && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <div className={classes.toolbarButtons}>
            <Tooltip title="Добавить">
              <IconButton color="inherit" onClick={handleLogout}>
                <AddCircleOutlineIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Редактировать">
              <IconButton color="inherit" onClick={handleLogout}>
                <CreateIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Обновить">
              <IconButton color="inherit" onClick={handleLogout}>
                <RefreshIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Нстройка">
              <IconButton color="inherit" onClick={handleOpenSettings}>
                <TuneIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Выйти">
              <IconButton color="inherit" onClick={handleLogout}>
                <ExitToAppIcon />
              </IconButton>
            </Tooltip>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !openMainMenu && classes.drawerPaperClose),
        }}
        open={openMainMenu}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          <MenuList />
        </List>
        <Divider />
      </Drawer>
    </>
  );
};

export default AppBarComponent;
