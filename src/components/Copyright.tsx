import React, { FC } from 'react';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  copyright: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
}));

const Copyright: FC = () => {
  const styles = useStyles();

  return (
    <div className={styles.copyright}>
      <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://krit.pro/" target="_blank">
          KRIT
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    </div>
  );
};

export default Copyright;
