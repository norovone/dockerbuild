import React, { FC, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { IColumnTableOrders, IDataTableOrders } from '../types/elementsTypes';
import { colunmsTableOrders, rowsTableOrders } from '../assets/elemetsData/elemetsData';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { useActions } from '../hooks/useActions';

const utils = (columnsTable: any, colunmsTableOrders: any) => {
  const colunmsChecked = columnsTable.reduce((acc: any, item: any) => {
    acc[item] = item;

    return acc;
  }, {});

  const colunmsFiltered = colunmsTableOrders.filter((item: any) => colunmsChecked[item.idx]);

  return colunmsFiltered;
};

const rows: IDataTableOrders[] = rowsTableOrders;

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
});

const StickyHeadTable: FC = () => {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const { columnsTable } = useTypedSelector((state) => state.settingsTable);
  const { tableColumnChange } = useActions();
  const columns: IColumnTableOrders[] = columnsTable.length
    ? utils(columnsTable, colunmsTableOrders)
    : colunmsTableOrders;
  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  useEffect(() => {
    const columnsTableLS = localStorage.getItem('columnsTable');

    if (columnsTableLS) {
      try {
        const correctColumnTableLS = JSON.parse(columnsTableLS);

        tableColumnChange(correctColumnTableLS);
      } catch (e) {
        throw new Error('Некорретный список столбцов');
      }
    }
  }, []);

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.number}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default StickyHeadTable;
