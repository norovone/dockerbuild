import React, { FC } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useTypedSelector } from '../hooks/useTypedSelector';
import CloseIcon from '@material-ui/icons/Close';
import { useActions } from '../hooks/useActions';
import { Button, Grid, Typography } from '@material-ui/core';
import _uniqueId from 'lodash/uniqueId';
import AddIcon from '@material-ui/icons/Add';
import { filters as filtersElem, compare as compareElem } from '../assets/elemetsData';
import Select from './UI/Select';
import Input from './UI/Input';

const useStyles = makeStyles({
  root: {
    width: '100%',
    padding: '10px',
  },
  modal: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'fixed',
    right: '5px',
    width: '720px',
    height: '100vh',
    padding: '20px',
    zIndex: 99,
    backgroundColor: 'white',
    boxShadow: '-0.3em 0 .7em lightgrey',
  },
  closeIcon: {
    cursor: 'pointer',
  },
  filters: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  filter: {
    marginRight: '5px',
  },
  filterButton: {
    display: 'flex',
    justifyContent: 'space-between',
  },
});
const TableFilters: FC = () => {
  const classes = useStyles();
  const { openFilters, filters } = useTypedSelector((state) => state.settingsTable);
  const { filtersClose, filterAdd } = useActions();
  const [filterDict, setFilterDict] = React.useState({});
  const [filterSelected, setFilterSelected] = React.useState('');
  const [compareSelected, setCompareSelected] = React.useState('');
  const [valueSelected, setValueSelected] = React.useState('');

  const handleFiltersClose = () => {
    filtersClose();
  };

  const handleAddFilter = () => {
    const idFilter = _uniqueId();

    filterAdd({
      id: 'filter-' + idFilter,
      options: filtersElem,
      compareId: 'compare-' + idFilter,
      compareElem: compareElem,
      valueId: 'value-' + idFilter,
      value: '',
    });
  };

  const setDict = (idFilter: string, name: string) => {
    const idSplit = idFilter.split('-');
    const id = Number(idSplit[1]);
    const key = idSplit[0];

    setFilterDict((prevState: any) => ({
      ...prevState,
      [id]: { ...prevState[id], [key]: name },
    }));
  };
  console.log(filterDict);
  const handleChange = async (event: React.ChangeEvent<{ name: unknown; value: unknown }>) => {
    const name = event.target.name as string;
    const selected = event.target.value as string;
    setFilterSelected(selected);

    setDict(name, selected);
  };

  const handleChangeCompare = (event: React.ChangeEvent<{ name: unknown; value: unknown }>) => {
    const name = event.target.name as string;
    const selected = event.target.value as string;
    setCompareSelected(selected);

    setDict(name, selected);
  };

  const handleChangeValue = (event: React.ChangeEvent<{ name: unknown; value: unknown }>) => {
    const name = event.target.name as string;
    const selected = event.target.value as string;
    setValueSelected(selected);

    setDict(name, selected);
  };

  return (
    <>
      {openFilters && (
        <div className={classes.modal}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <CloseIcon className={classes.closeIcon} onClick={handleFiltersClose} />
            </Grid>
            <Grid item xs={12}>
              <Typography component="h1" variant="h5">
                Настройка фильтра
              </Typography>
              <div className={classes.filters}>
                {filters.map((item) => (
                  <div key={item.id}>
                    <Select
                      key={item.id}
                      options={filtersElem}
                      id={item.id}
                      name={item.id}
                      autoComplete={item.id}
                      onChange={handleChange}
                      className={classes.filter}
                    />
                    <Select
                      key={item.compareId}
                      options={item.compareElem}
                      id={item.compareId}
                      name={item.compareId}
                      autoComplete={item.compareId}
                      onChange={handleChangeCompare}
                      className={classes.filter}
                    />
                    <Input
                      key={item.valueId}
                      id={item.valueId}
                      name={item.valueId}
                      onChange={handleChangeValue}
                    />
                  </div>
                ))}
              </div>
            </Grid>
            <Grid item xs={12}>
              <div className="filterButton">
                <Button onClick={handleAddFilter} color="primary" variant="contained">
                  <AddIcon /> Добавить поле
                </Button>
                <Button color="primary" variant="contained">
                  Применить
                </Button>
              </div>
            </Grid>
          </Grid>
        </div>
      )}
    </>
  );
};

export default TableFilters;
