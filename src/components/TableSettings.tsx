import React, { FC } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useTypedSelector } from '../hooks/useTypedSelector';
import CloseIcon from '@material-ui/icons/Close';
import { useActions } from '../hooks/useActions';
import { Grid, Paper, Typography } from '@material-ui/core';
import TableSettingsList from './TableSettingsList';

const useStyles = makeStyles({
  root: {
    width: '100%',
    padding: '10px',
  },
  modal: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'fixed',
    right: '5px',
    width: '320px',
    height: '100vh',
    padding: '20px',
    zIndex: 99,
    backgroundColor: 'white',
    boxShadow: '-0.3em 0 .7em lightgrey',
  },
  closeIcon: {
    cursor: 'pointer',
  },
});
const TableSettings: FC = () => {
  const classes = useStyles();
  const { openSettingsTable } = useTypedSelector((state) => state.settingsTable);
  const { settingsTableClose } = useActions();

  const handleCloseSetingsTable = () => {
    settingsTableClose();
  };

  return (
    <>
      {openSettingsTable && (
        <div className={classes.modal}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <CloseIcon className={classes.closeIcon} onClick={handleCloseSetingsTable} />
            </Grid>
            <Grid item xs={12}>
              <Typography component="h1" variant="h5">
                Настройка таблицы
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography component="h5">Поля</Typography>
              <Paper className={classes.root}>
                <TableSettingsList />
              </Paper>
            </Grid>
          </Grid>
        </div>
      )}
    </>
  );
};

export default TableSettings;
