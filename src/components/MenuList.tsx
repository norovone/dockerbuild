import React, { FC } from 'react';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import SettingsIcon from '@material-ui/icons/Settings';
import HelpIcon from '@material-ui/icons/Help';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { makeStyles } from '@material-ui/core/styles';
import { config } from '../config';
import MenuItems from './MenuItems';
import { Badge } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  menuitem: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
}));

const items = [
  {
    text: 'Провиль',
    icon: <AccountCircleIcon />,
    route: config.PROFILE,
  },
  {
    text: 'Оповещения',
    icon: (
      <Badge badgeContent={4} color="primary">
        <NotificationsNoneIcon />
      </Badge>
    ),
    route: config.ORDERS,
  },
  {
    text: 'Настройки',
    icon: <SettingsIcon />,
    route: config.SETTINGS,
  },
  {
    text: 'Помощь',
    icon: <HelpIcon />,
    route: config.HELP,
  },
  {
    text: 'Выход',
    icon: <ExitToAppIcon />,
    route: config.LOGIN,
  },
];

const MenuList: FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.menuitem}>
      {items.map((item) => (
        <MenuItems key={item.route} item={item} />
      ))}
    </div>
  );
};

export default MenuList;
