import React, { FC } from 'react';

interface ErrorProps {
  error: string;
}

const Error: FC<ErrorProps> = (props) => {
  return (
    <div>
      <h1>Error</h1>
      <p>{props.error}</p>
    </div>
  );
};

export default Error;
