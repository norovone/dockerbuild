import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import AppBar from '../AppBar';
import { shallow } from 'enzyme';

const mockStore = configureStore();

describe('<AppBar />', () => {
  let wrapper: any;

  it('defines the component', () => {
    wrapper = shallow(
      <Provider store={mockStore()}>
        <AppBar />
      </Provider>
    );

    expect(wrapper).toBeDefined();
  });
});
