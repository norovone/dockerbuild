import React, { FC } from 'react';
import { NavLink } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import { Tooltip } from '@material-ui/core';
import { useTypedSelector } from '../hooks/useTypedSelector';

const useStyles = makeStyles((theme) => ({
  nested: {
    paddingLeft: theme.spacing(4),
  },
  link: {
    textDecoration: 'none',
    color: theme.palette.text.primary,
  },
}));

interface IItem {
  icon: object;
  route: string;
  text: string;
}

interface MenuItemsProps {
  item: IItem;
}

const MenuItems: FC<MenuItemsProps> = ({ item }) => {
  const classes = useStyles();
  const { openMainMenu } = useTypedSelector((state) => state.menu);

  return (
    <div>
      <NavLink to={item.route} className={classes.link}>
        <Tooltip title={!openMainMenu ? item.text : ''}>
          <ListItem button className={classes.nested}>
            <ListItemIcon>{item.icon}</ListItemIcon>
            <ListItemText primary={item.text} />
          </ListItem>
        </Tooltip>
      </NavLink>
    </div>
  );
};

export default MenuItems;
