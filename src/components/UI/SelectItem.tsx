import React, { FC } from 'react';

interface IOptions {
  id: string;
  title: string;
}

interface ISelectItemProps {
  options?: any;
}

const SelectItem: FC<ISelectItemProps> = ({ options }) => {
  return (
    <>
      {options &&
        Array.isArray(options) &&
        options.map((option) => (
          <option key={option.id} value={option.id}>
            {option.label}
          </option>
        ))}
    </>
  );
};

export default SelectItem;
