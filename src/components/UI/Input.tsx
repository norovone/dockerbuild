import React, { FC, forwardRef, PropsWithChildren } from 'react';
import { TextField } from '@material-ui/core';

type IProps = {
  [key: string]: any;
};

const Input: FC<IProps> = (props) => {
  return (
    <TextField
      variant="outlined"
      margin="normal"
      onChange={props.onChange}
      inputRef={props.ref}
      {...props}
    />
  );
};

export default Input;
