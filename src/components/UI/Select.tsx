import React, { FC } from 'react';
import TextField from '@material-ui/core/TextField';
import SelectItem from './SelectItem';

type IProps = {
  [key: string]: any;
};

const Select: FC<IProps> = ({ id, options, onChange, value, register, props }) => {
  return (
    <TextField
      select
      variant="outlined"
      margin="normal"
      required
      onChange={onChange}
      value={value}
      {...props}
      SelectProps={{
        native: true,
        inputProps: { ref: register, name: id },
      }}
      defaultValue=""
    >
      <SelectItem key={id} options={options} />
    </TextField>
  );
};

export default Select;
