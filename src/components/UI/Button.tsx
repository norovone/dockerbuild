import React, { FC } from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(3, 0, 2),
  },
}));

type IProps = {
  [key: string]: any;
};

const ButtonComponent: FC<IProps> = ({ children, props }) => {
  const classes = useStyles();

  return (
    <Button
      // type="submit"
      variant="contained"
      color="primary"
      className={classes.root}
      {...props}
    >
      {children}
    </Button>
  );
};

export default ButtonComponent;
