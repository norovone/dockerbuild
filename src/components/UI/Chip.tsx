import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      flexWrap: 'wrap',
      '& > *': {
        margin: theme.spacing(0.5),
      },
    },
  })
);

export default function Chips() {
  const classes = useStyles();

  const handleDelete = () => {
    console.info('You clicked the delete icon.');
  };

  const handleClick = () => {
    console.info('You clicked the Chip.');
  };

  return (
    // <div className={classes.root}>
    <div>
      <Chip label="фильтр 1" onClick={handleClick} onDelete={handleDelete} />
      <Chip label="фильтр 2" onClick={handleClick} onDelete={handleDelete} />
      <Chip label="фильтр 3" onClick={handleClick} onDelete={handleDelete} />
    </div>
  );
}
