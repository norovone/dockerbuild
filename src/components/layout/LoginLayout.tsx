import React, { FC } from 'react';

interface ILayoutProps {
  children: React.ReactNode;
}

const LoginLayout: FC<ILayoutProps> = ({ children }) => {
  return <div>{children}</div>;
};

export default LoginLayout;
