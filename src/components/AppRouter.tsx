import React, { FC } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { privateRoutes, publicRoutes } from '../routes';
import { config } from '../config';
import { useTypedSelector } from '../hooks/useTypedSelector';
import Loader from './Loader';
import Error from './Error';

const AppRouter: FC = () => {
  const { auth, error, loading } = useTypedSelector((state) => state.auth);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return <Error error={error} />;
  }

  return auth ? (
    <Switch>
      {privateRoutes.map(({ path, Component }) => (
        <Route key={path} path={path} component={Component} exact={true} />
      ))}

      <Redirect to={config.ORDERS} />
    </Switch>
  ) : (
    <Switch>
      {publicRoutes.map(({ path, Component }) => (
        <Route key={path} path={path} component={Component} exact={true} />
      ))}

      <Redirect to={config.LOGIN} />
    </Switch>
  );
};

export default AppRouter;
