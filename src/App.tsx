import React, { FC, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { useTypedSelector } from './hooks/useTypedSelector';
import MainLayout from './components/layout/MainLayout';
import AppRouter from './components/AppRouter';
import LoginLayout from './components/layout/LoginLayout';
import { useActions } from './hooks/useActions';
import Loader from './components/Loader';
import Error from './components/Error';

const App: FC = () => {
  const { auth, error, loading } = useTypedSelector((state) => state.auth);

  // Временный костыль авторизации пока нет авторизации с бэкенда
  const { fetchLogin } = useActions();
  useEffect(() => {
    if (localStorage.getItem('auth')) {
      fetchLogin();
    }
  }, []);
  // Временный костыль авторизации пока нет авторизации с бэкенда

  if (loading) {
    return <Loader />;
  }

  if (error) {
    return <Error error={error} />;
  }

  return (
    <div>
      <Router>
        {auth ? (
          <MainLayout>
            <AppRouter />
          </MainLayout>
        ) : (
          <LoginLayout>
            <AppRouter />
          </LoginLayout>
        )}
      </Router>
    </div>
  );
};

export default App;
