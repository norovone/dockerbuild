import React, { FC } from 'react';
import { Grid, Paper, Typography, makeStyles, Button } from '@material-ui/core';
import Table from '../components/Table';
import Tabs from '../components/Tabs';
import Input from '../components/UI/Input';
import Chip from '../components/UI/Chip';
import HistoryPath from '../components/UI/Breadcrumb';
import { useActions } from '../hooks/useActions';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexGrow: 1,
  },
  table: {
    width: 'auto',
  },
  filters: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
}));

const Orders: FC = () => {
  const classes = useStyles();
  const { filtersOpen } = useActions();

  const handleOpenFilters = () => {
    filtersOpen();
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper>
            <HistoryPath />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper>
            <Grid container spacing={3} className={classes.filters}>
              <Grid item xs={3}>
                <Button onClick={handleOpenFilters} color="primary" variant="contained">
                  Добавить фильтр
                </Button>
              </Grid>
              <Grid item xs={3}>
                <Chip />
              </Grid>
              <Grid item xs={3}>
                <Input placeholder="Название" />
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Tabs />
          <Table />
        </Grid>
      </Grid>
    </div>
  );
};

export default Orders;
